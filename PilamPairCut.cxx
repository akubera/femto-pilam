/* 
 * File:   PilamPairCut.cxx
 * Author: akubera
 * 
 * Created on September 29, 2014, 2:51 PM
 */

#include "PilamPairCut.h"

#include <sstream>
#include <TObjArray.h>

static const double PionMass = 0.13956995,
                    KaonMass = 0.493677,
                  ProtonMass = 0.938272013,
                  LambdaMass = 1.115683,
                      XiMass = 1.31486;

static const int LambdaPurityBinCount = 288*2;
static const double LambdaPurityHistLow = 1.09,
                    LambdaPurityHistHigh = 1.14;

static const int PionPurityBinCount = 288;
static const double PionPurityHistLow = 0,
                    PionPurityHistHigh = 1;


static const int DEFAULT_MASS_NBINS = 288;

static const TMap *DEFAULT_PARAM_MAP = NULL;


TMap*
get_defaults()
{
  if (DEFAULT_PARAM_MAP == NULL) {
    DEFAULT_PARAM_MAP = new TMap();
    DEFAULT_PARAM_MAP->Add(new TString("LambdaPurityHist"), new TVectorD(3, {288, 0, 8}));
  }
  return DEFAULT_PARAM_MAP;
}

PilamPairCut::PilamPairCut():
  AliFemtoV0TrackPairCut(),
  _pion_type(PilamMultiAnalysis::kPiPlus),
  _lambda_type(PilamMultiAnalysis::kLambda),
  lambda_purity_window(LambdaPurityHistLow, LambdaPurityHistHigh) // LambdaMass + 0.5)
{
  _lam_purity_hist = new TH1D("LambdaPurity", "Lambda Purity", LambdaPurityBinCount, LambdaPurityHistLow, LambdaPurityHistHigh);
  _minv_pass = new TH1D("Mass_Pass", "M_{inv} (Pass);M_{inv} (GeV)", DEFAULT_MASS_NBINS, 0, 8);
  _minv_fail = new TH1D("Mass_Fail", "M_{inv} (Fail);M_{inv} (GeV)", DEFAULT_MASS_NBINS, 0, 8);

}

PilamPairCut::PilamPairCut(PilamMultiAnalysis*,
                           const PilamMultiAnalysis::ParticleType& piontype,
                           const PilamMultiAnalysis::ParticleType& lambdatype,
                           TMap *options):
  AliFemtoV0TrackPairCut(),
  _pion_type(piontype),
  _lambda_type(lambdatype),
  lambda_purity_window(LambdaPurityHistLow, LambdaPurityHistHigh)
{
  TVectorD *hist_info = (hist_info)get_defaults()->FindObject("LambdaPurityHist");
  
  _lam_purity_hist = new TH1D("lambda_purity", "Lambda Purity; M_{inv} (GeV)", LambdaPurityBinCount, LambdaPurityHistLow, LambdaPurityHistHigh);
  _pi_purity_hist =  new TH1D("pion_purity", "Pion Purity; purity?;", PionPurityBinCount, PionPurityHistLow, PionPurityHistHigh);
  _minv_pass = new TH1D("Mass_Pass", "M_{inv} (Pass);M_{inv} (GeV)", 288, 0, 8);
  _minv_fail = new TH1D("Mass_Fail", "M_{inv} (Fail);M_{inv} (GeV)", 288, 0, 8);

}

PilamPairCut::PilamPairCut(const PilamPairCut& orig):
  AliFemtoV0TrackPairCut(orig),
  lambda_purity_window(orig.lambda_purity_window)
{
  _lam_purity_hist = (TH1D*)orig._lam_purity_hist->Clone();
}

PilamPairCut::~PilamPairCut()
{
  delete _lam_purity_hist;
  delete _pi_purity_hist;
  delete _minv_pass;
  delete _minv_fail;
}

bool
PilamPairCut::Pass (const AliFemtoPair* pair)
{
  const double minv = pair->MInv();


  bool track1_is_lambda = pair->Track1()->V0() != NULL;

  AliFemtoParticle *lambda = track1_is_lambda ? pair->Track1() : pair->Track2(),
                     *pion = track1_is_lambda ? pair->Track2() : pair->Track1();

  double lam_mass = _lambda_type == PilamMultiAnalysis::kLambda ? lambda->V0()->MassLambda() : lambda->V0()->MassAntiLambda();
  
  if (lambda_purity_window.first <= lam_mass  && lam_mass <= lambda_purity_window.second) {
    _lam_purity_hist->Fill(lam_mass);
  }

  _pi_purity_hist->Fill(pion->GetPionPurity());

  bool passed = AliFemtoV0TrackPairCut::Pass(pair);
  passed &= ::fabs(lam_mass - LambdaMass) <= 0.0038;

  (passed ? _minv_pass : _minv_fail)->Fill(minv);

  return passed;
}

AliFemtoString
PilamPairCut::Report()
{
  std::stringstream s;
  s << "[" << PilamPairCut::Report() << "]";
  return AliFemtoString(s.str());
}

TList *
PilamPairCut::ListSettings()
{
//  cout << "[PilamPairCut::ListSettings]\n";
  return new TList();
}

TList*
PilamPairCut::GetOutputList()
{
//  cout << "[PilamPairCut::GetOutputList]\n";
  TList *olist = new TList();
  TList* monitor_list = AliFemtoCutMonitorHandler::GetOutputList();
  TObjArray *array = new TObjArray();
  array->SetName("PairCut");
  TIter next(monitor_list);
  while (TObject *obj = next()) {
    array->Add(obj);
  }
  array->Add(_lam_purity_hist);
  array->Add(_pi_purity_hist);
  array->Add(_minv_pass);
  array->Add(_minv_fail);

  olist->Add(array);
  return olist;

  return new TList();
}

/*
AliFemtoPairCut *
PilamPairCut::Clone()
{
  return new PilamPairCut(*this);
}
*/
