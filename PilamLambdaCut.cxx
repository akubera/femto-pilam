/* 
 * File:   PilamLambdaCut.cpp
 * Author: akubera
 * 
 * Created on October 13, 2014, 5:14 PM
 */

#include "PilamLambdaCut.h"
#include "PilamMultiAnalysis.h"

#include <TList.h>
#include <TObjArray.h>

#ifdef __ROOT__
  ClassImp(PilamLambdaCut)
#endif

static const double LambdaMass = 1.115683,
               PurityWindowMin = LambdaMass - 0.6,
               PurityWindowMax = LambdaMass + 1.6;

PilamLambdaCut::PilamLambdaCut():
  AliFemtoV0TrackCut(),
  lambda_purity_window(PurityWindowMin, PurityWindowMax),
  _analysis(NULL),
  _type(PilamMultiAnalysis::kLambda),
  _name("LambdaCut"),
  _lambda_purity(NULL),
  _minv_pass(new TH1D("Minv_Pass", "M_{inv} (Passed)", 1000, 0, 8)),
  _minv_fail(new TH1D("Minv_Fail", "M_{inv} (Failed)", 1000, 0, 8))
{
  /* no-op */
}

PilamLambdaCut::PilamLambdaCut(PilamMultiAnalysis *parent, const PilamMultiAnalysis::ParticleType& type):
  AliFemtoV0TrackCut(),
  lambda_purity_window(LambdaMass - 0.6, LambdaMass + 1.6),
  _analysis(parent),
  _type(type),
  _name("LambdaCut"),
  _lambda_purity(new TH1D("Lambda", "Lambda Purity; M_{inv}", 1000, 1.05, 1.3)),
  _minv_pass(new TH1D("Minv_Pass", "M_{inv} (Passed)", 1000, 0, 8)),
  _minv_fail(new TH1D("Minv_Fail", "M_{inv} (Failed)", 1000, 0, 8))
{
  switch (_type) {
    case PilamMultiAnalysis::kLambda:
      SetParticleType(0 /*AliFemtoV0TrackCut::V0Type::kLambda*/);
      break;
    case PilamMultiAnalysis::kAntiLambda:
      SetParticleType(1 /*AliFemtoV0TrackCut::V0Type::kAntiLambda*/);
      break;
    default:
      std::cerr << "Error : Incorrect lambda type: '" << _type << "'\n";
      throw 1;
  }

}

/*
PilamLambdaCut::PilamLambdaCut(const PilamLambdaCut& orig):
   AliFemtoV0TrackCut(const_cast<PilamLambdaCut&>(orig)),
  _analysis(orig._analysis),
  _type(orig._type)
{
  _lambda = (TH1D)orig._pion->Clone();
}
*/

PilamLambdaCut::~PilamLambdaCut()
{
  delete _lambda_purity;
}

bool
PilamLambdaCut::Pass(const AliFemtoV0* track)
{
  double minv = _type == PilamMultiAnalysis::kLambda ? track->MassLambda() : track->MassAntiLambda();

//  if (lambda_purity_window.first <= minv  && minv <= lambda_purity_window.second) {
//    _lambda_purity->Fill(minv);
//  }
  bool passed = AliFemtoV0TrackCut::Pass(track);
  
  (passed ? _minv_pass : _minv_fail)->Fill(minv);
  
  return passed;
}

TList*
PilamLambdaCut::GetOutputList()
{
//  cout << "[PilamLambdaCut::GetOutputList]\n";
  TList *olist = new TList();
  TList* monitor_list = AliFemtoCutMonitorHandler::GetOutputList();
  TObjArray *array = new TObjArray();
  array->SetName(_name.Data());
  TIter next(monitor_list);
  while (TObject *obj = next()) {
    array->Add(obj);
  }
  array->Add(_lambda_purity);
  array->Add(_minv_pass);
  array->Add(_minv_fail);
  olist->Add(array);
  return olist;
}

void
PilamLambdaCut::Finish()
{
  cout << "[PilamLambdaCut::Finish]\n";
  AliFemtoV0TrackCut::Finish();
//  _lambda_purity->Scale(1.0/_lambda_purity->GetBinWidth(1));
  _lambda_purity->Scale(1.0/_lambda_purity->GetXaxis()->GetBinWidth(4));
}
