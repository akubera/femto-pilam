/* 
 * File:   PilamLambdaCut.h
 *
 */

#ifndef PILAM_LAMBDACUT_H
#define	PILAM_LAMBDACUT_H

#include <AliFemtoV0TrackCut.h>

#include <utility>
#include <TH1D.h>

#include "PilamMultiAnalysis.h"

class PilamLambdaCut : public AliFemtoV0TrackCut {
public:
  PilamLambdaCut();
  PilamLambdaCut(PilamMultiAnalysis *parent, const PilamMultiAnalysis::ParticleType&);
  virtual ~PilamLambdaCut();

  virtual bool Pass(const AliFemtoV0* track);

  virtual TList *GetOutputList();
  
  std::pair<double,double> lambda_purity_window;

  virtual void Finish();
  
protected:

  PilamMultiAnalysis *_analysis;
  PilamMultiAnalysis::ParticleType _type;

  TString _name;

  TH1D *_lambda_purity;

  TH1D *_minv_pass;
  TH1D *_minv_fail;

#ifdef __ROOT__
  ClassDef(PilamLambdaCut, 0)
#endif
  
};

#endif	/* PILAM_PIONCUT_H */

