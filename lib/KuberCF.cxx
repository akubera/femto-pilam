/*
 *  KuberCF.cxx
 *
 */

#ifndef KUBERCF_CXX
#define KUBERCF_CXX

#include <AliFemtoCorrFctn.h>

//
#include "KuberCF.h"

#include <iostream>
#include <sstream>

#ifdef __ROOT__
ClassImp(KuberCF)
#endif

KuberCF::KuberCF(): AliFemtoCorrFctn() {}

KuberCF::KuberCF(char *title, const int& nbins, const float KStarLo, const float KStarHi, AliFemtoAnalysis *analysis):
  AliFemtoCorrFctn(),
  _numerator(0),
  _denominator(0)
{
  std::cout << "[KuberCF::KuberCF]\n";
  std::cout << "  Setting Analysis: " << analysis << "\n";
  SetAnalysis(analysis);
  std::stringstream ss;

  ss << title << "_Num";
  _numerator = new TH1D(ss.str().c_str(), title, nbins, KStarLo, KStarHi);
  _numerator->Sumw2();

  ss.str(std::string());
  ss.clear();
  ss << title << "_Den";
  _denominator = new TH1D(ss.str().c_str(), title, nbins, KStarLo, KStarHi);
  _denominator->Sumw2();
}


AliFemtoString
KuberCF::Report()
{

  std::stringstream ss;
  ss << "[KuberCF::Report]\n";
  ss << "  # in numerator : " << _numerator->GetEntries() << "\n";
  ss << "  # in denominator : " << _denominator->GetEntries() << "\n";
  AliFemtoString res = ss.str();
  return res;
}

void
KuberCF::Finish()
{

}

TList*
KuberCF::GetOutputList()
{
  TList *olist = new TList();
  olist->Add(_numerator);
  olist->Add(_denominator);
  return olist;
}


void
KuberCF::AddRealPair(AliFemtoPair* aPair)
{
  if (fPairCut && !fPairCut->Pass(aPair)) {
    return;
  }

  double kstar = aPair->KStar();
  _numerator->Fill(kstar);

}

void
KuberCF::AddMixedPair(AliFemtoPair* aPair)
{
  if (fPairCut && !fPairCut->Pass(aPair)) {
    return;
  }

  double kstar = aPair->KStar();
  _denominator->Fill(kstar);
}

/*
void
KuberCF::EventBegin(const AliFemtoEvent* // aEvent
)
{
}

void
KuberCF::EventEnd(const AliFemtoEvent* // aEvent
)
{
}
// */

#endif /* KUBERCF_CXX */
