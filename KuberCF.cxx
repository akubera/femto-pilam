/*
 *  KuberCF.cxx
 *
 */

#ifndef KUBERCF_CXX
#define KUBERCF_CXX

#include <AliFemtoCorrFctn.h>

//
#include "KuberCF.h"

#include <iostream>
#include <sstream>

#ifdef __ROOT__
ClassImp(KuberCF)
#endif

double AliFemtoPair_DCAInsideTpc(const AliFemtoPair *pair);

KuberCF::KuberCF(): AliFemtoCorrFctn() {}

KuberCF::KuberCF(char *title, const int& nbins, const float KStarLo, const float KStarHi):
  AliFemtoCorrFctn(),
  _numerator(0),
  _denominator(0),
  _minv(0),
  _qinv(0),
  _minv_m(0),
  _qinv_m(0)
{
  // std::cout << "[KuberCF::KuberCF]\n";
  // std::cout << "  Setting Analysis: " << analysis << "\n";
  // SetAnalysis(analysis);

  std::stringstream ss;

  ss << title << "_Num";
  _numerator = new TH1D(ss.str().c_str(), title, nbins, KStarLo, KStarHi);
  _numerator->Sumw2();

  ss.str(std::string());
  ss.clear();
  ss << title << "_Den";
  _denominator = new TH1D(ss.str().c_str(), title, nbins, KStarLo, KStarHi);
  _denominator->Sumw2();

  float mmin = 0, mmax = 8.0;
  
  ss.str(std::string());
  ss.clear();
  ss << title << "_minv";
  _minv = new TH1D(ss.str().c_str(), "m_{inv};m_{inv} (GeV)", nbins, mmin,mmax);
  _minv->Sumw2();
  
  ss.str(std::string());
  ss.clear();
  ss << title << "_minv_m";
  _minv_m = new TH1D(ss.str().c_str(), "m_{inv} (mixed events);m_{inv} (GeV)", nbins, mmin,mmax);
  _minv_m->Sumw2();

  ss.str(std::string());
  ss.clear();
  ss << title << "_qinv";
  _qinv = new TH1D(ss.str().c_str(), "q_{inv};q_{inv} (GeV)", nbins, mmin,mmax);
  _qinv->Sumw2();

  /*
  ss.str(std::string());
  ss.clear();
  ss << title << "_qinv_m";
  _qinv_m = new TH1D(ss.str().c_str(), "q_{inv} (mixed events);q_{inv} (GeV)", nbins, mmin,mmax);
  _qinv_m->Sumw2();

  ss.clear();
  ss << title << " :: DcaInsideTpc";
  
  int nBins = 200;
  _pair_dca = new TH1D("DCA_Pass" , ss.str().c_str(), nBins, 0, 20);
  ss << " (fail)";
  _pair_dca_fail = new TH1D("DCA_Fail", ss.str().c_str(), nBins, 0, 20);
  */
}


AliFemtoString
KuberCF::Report()
{
  std::stringstream ss;
  ss << "[KuberCF::Report]\n";
  ss << "  # in numerator : " << _numerator->GetEntries() << "\n";
  ss << "  # in denominator : " << _denominator->GetEntries() << "\n";
  AliFemtoString res = ss.str();
  return res;
}

void
KuberCF::Finish()
{

}

TList*
KuberCF::GetOutputList()
{
  TList *olist = new TList();
  
  
  TObjArray *toa = new TObjArray();
  toa->SetName("PairInformation");
  toa->Add(_pair_dca);
  toa->Add(_pair_dca_fail);
  
  olist->Add(_numerator);
  olist->Add(_denominator);
  olist->Add(_minv);
  olist->Add(_minv_m);
//  olist->Add(_qinv);
//  olist->Add(_qinv_m);
  return olist;
}


void
KuberCF::AddRealPair(AliFemtoPair* aPair)
{
  if (fPairCut && !fPairCut->Pass(aPair)) {
//    _pair_dca_fail->Fill(aPair->GetWeightedAvSep());
//    _pair_dca_fail->Fill(AliFemtoPair_DCAInsideTpc(aPair));
//    _pair_dca_fail->Fill(aPair->NominalTpcEntranceSeparation())
    return;
  }
//  _pair_dca->Fill(aPair->GetWeightedAvSep());
//  _pair_dca->Fill(AliFemtoPair_DCAInsideTpc(aPair));
//  _pair_dca->Fill(aPair->NominalTpcEntranceSeparation())

  double kstar = aPair->KStar();
  _numerator->Fill(kstar);

  _minv->Fill(aPair->MInv());
  // _qinv->Fill(aPair->QInv());
}

void
KuberCF::AddMixedPair(AliFemtoPair* aPair)
{
  if (fPairCut && !fPairCut->Pass(aPair)) {
    return;
  }
  double kstar = aPair->KStar();
  _denominator->Fill(kstar);

  _minv_m->Fill(aPair->MInv());
  // _qinv_m->Fill(aPair->QInv());

}

/*
void
KuberCF::EventBegin(const AliFemtoEvent* // aEvent
)
{
}

void
KuberCF::EventEnd(const AliFemtoEvent* // aEvent
)
{
}
*/


double AliFemtoPair_DCAInsideTpc(const AliFemtoPair *pair)
{
   // dcs inside the STAR TPC
   double tMinDist = pair->NominalTpcEntranceSeparation();
   double tExit = pair->NominalTpcExitSeparation();
   tMinDist = (tExit>tMinDist) ? tMinDist : tExit;
   double tInsideDist;
   //tMinDist = 999.;

   double rMin = 60.;
   double rMax = 190.;
   const AliFmPhysicalHelixD& tHelix1 = pair->Track1()->Helix();
   const AliFmPhysicalHelixD& tHelix2 = pair->Track2()->Helix();
    
//   --- One is a line and other one a helix
   if (fabs(tHelix1.Curvature()) <= numeric_limits<double>::epsilon() ^ fabs(tHelix2.Curvature()) <= numeric_limits<double>::epsilon()) {
     return -999.;
   }
   // --- 2 lines : don't care right now
   //if (tHelix1.mSingularity)  return -999.;
   // --- 2 helix
   double dx = tHelix2.XCenter() - tHelix1.XCenter();
   double dy = tHelix2.YCenter() - tHelix1.YCenter();
   double dd = ::sqrt(dx*dx + dy*dy);
   double r1 = 1/tHelix1.Curvature();
   double r2 = 1/tHelix2.Curvature();
   double cosAlpha = (r1*r1 + dd*dd - r2*r2)/(2*r1*dd);
    
   double x, y, r;
   double s;
   if (fabs(cosAlpha) < 1) {           // two solutions
     double sinAlpha = sin(acos(cosAlpha));
     x = tHelix1.XCenter() + r1*(cosAlpha*dx - sinAlpha*dy)/dd;
     y = tHelix1.YCenter() + r1*(sinAlpha*dx + cosAlpha*dy)/dd;
     r = ::sqrt(x*x+y*y);
     if( r > rMin &&  r < rMax && 
 	fabs(atan2(y,x)-pair->Track1()->Track()->NominalTpcEntrancePoint().Phi())< 0.5
 	){ // first solution inside
       s = tHelix1.PathLength(x, y);
       tInsideDist=tHelix2.Distance(tHelix1.At(s));
       if(tInsideDist<tMinDist) tMinDist = tInsideDist;
     }
     else{ 
       x = tHelix1.XCenter() + r1*(cosAlpha*dx + sinAlpha*dy)/dd;
       y = tHelix1.YCenter() + r1*(cosAlpha*dy - sinAlpha*dx)/dd;
       r = ::sqrt(x*x+y*y);
       if( r > rMin &&  r < rMax &&
 	  fabs(atan2(y,x)-pair->Track1()->Track()->NominalTpcEntrancePoint().Phi())< 0.5
 	  ) {  // second solution inside
         s = tHelix1.PathLength(x, y);
         tInsideDist=tHelix2.Distance(tHelix1.At(s));
         if(tInsideDist<tMinDist) tMinDist = tInsideDist;
       }     
     }
   }
   return tMinDist;
 }
#endif /* KUBERCF_CXX */
