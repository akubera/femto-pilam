/* 
 * File:   PilamEventCut.cpp
 * Author: akubera
 * 
 * Created on October 13, 2014, 5:14 PM
 */

#include "PilamEventCut.h"
#include "PilamMultiAnalysis.h"

#include <TList.h>
#include <TObjArray.h>

#ifdef __ROOT__
  ClassImp(PilamEventCut)
#endif

PilamEventCut::PilamEventCut():
  AliFemtoBasicEventCut()
{ /* no-op */
  cout << "[PilamEventCut()]\n";
}

PilamEventCut::PilamEventCut(PilamMultiAnalysis *parent, float centrality_low, float centrality_high):
//  AliFemtoBasicEventCut(),
  _analysis(parent),
  _name("EventCut"),
  _title_prefix(parent->GetName()),
  _cen_low(centrality_low),
  _cen_high(centrality_high)
{
//  cout << "[PilamEventCut(PilamMultiAnalysis*, float, float)]\n";
  _centrality_pass = new TH1D("CentralityPass", (_title_prefix + " : Centrality Pass").Data(), 100, 0, 100.0);
  _centrality_fail = new TH1D("CentralityFail", (_title_prefix + " : Centrality Fail").Data(), 100, 0, 100.0);
}

PilamEventCut::PilamEventCut(const char *name, const char *title):
  AliFemtoBasicEventCut(),
  _name(name),
  _title_prefix(title),
  _cen_low(0.0),
  _cen_high(100.0)
{
  fCollectionsEmpty = 0;
  _centrality_pass = new TH1D("CentralityPass", (_title_prefix + " : Centrality Pass").Data(), 100, 0, 100.0);
  _centrality_fail = new TH1D("CentralityFail", (_title_prefix + " : Centrality Fail").Data(), 100, 0, 100.0);
}

PilamEventCut::PilamEventCut(const char *name, const char *title, float centrality_low, float centrality_high):
  AliFemtoBasicEventCut(),
  _name(name),
  _title_prefix(title),
  // _centrality_range(centrality_low, centrality_high),
  _cen_low(centrality_low),
  _cen_high(centrality_high)
{
  _centrality_pass = new TH1D("CentralityPass", (_title_prefix + " : Centrality Pass").Data(), 100, 0, 100.0);
  _centrality_fail = new TH1D("CentralityFail", (_title_prefix + " : Centrality Fail").Data(), 100, 0, 100.0);
}

PilamEventCut::PilamEventCut(const PilamEventCut& orig):
   AliFemtoBasicEventCut(const_cast<PilamEventCut&>(orig)),
  _name(orig._name),
  _title_prefix(orig._title_prefix),
  _cen_low(orig._cen_low),
  _cen_high(orig._cen_high)
{ /* no-op */
  _centrality_pass = (TH1D*)orig._centrality_pass->Clone();
  _centrality_fail = (TH1D*)orig._centrality_fail->Clone();
}

PilamEventCut::~PilamEventCut()
{
  delete _centrality_pass;
  delete _centrality_fail;
}

void
PilamEventCut::SetCentralityRange(float low, float high)
{
  //_centrality_range = std::make_pair(low,high);
  _cen_low = low;
  _cen_high = high;
}

bool
PilamEventCut::Pass(const AliFemtoEvent* event)
{
  float centrality = event->CentralityV0();
  /*
  cout << "--- Centrality ---\n\tV0 " << event->CentralityV0() << "\n";
  cout << "\tV0A " << event->CentralityV0A() << "\n";
  cout << "\tV0C " << event->CentralityV0C() << "\n";
  cout << "\tZNA " << event->CentralityZNA() << "\n";
  cout << "\tZNC " << event->CentralityZNC() << "\n";
  cout << "\tCL1 " << event->CentralityCL1() << "\n";
  cout << "\tCL0 " << event->CentralityCL0() << "\n";
  cout << "\tTKL " << event->CentralityTKL() << "\n";
  cout << "\tFMD " << event->CentralityFMD() << "\n";
  cout << "\tTrk " << event->CentralityTrk() << "\n";
  cout << "\tCND " << event->CentralityCND() << "\n";
  cout << "\tNPA " << event->CentralityNPA() << "\n";
  cout << "\tSPD1 " << event->CentralitySPD1() << "\n";
  */
//  if (centrality < _centrality_range.first || _centrality_range.second < centrality) {
  if (centrality < _cen_low || _cen_high <= centrality) {
    _centrality_fail->Fill(centrality);
    return kFALSE;
  }
  _centrality_pass->Fill(centrality);
  return AliFemtoBasicEventCut::Pass(event);
}

TList*
PilamEventCut::GetOutputList()
{
//  cout << "[PilamEventCut::GetOutputList]\n";
  TList *olist = new TList();
  TList* monitor_list = AliFemtoCutMonitorHandler::GetOutputList();
  TObjArray *array = new TObjArray();
  array->SetName(_name.Data());
  TIter next(monitor_list);
  while (TObject *obj = next()) {
    array->Add(obj);
  }
  array->Add(_centrality_pass);
  array->Add(_centrality_fail);
  olist->Add(array);
  return olist;
}
