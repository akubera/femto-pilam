/* 
 * File:   PilamEventCut.h
 * Author: akubera
 *
 * Created on October 13, 2014, 5:14 PM
 */

#ifndef PILAMEVENTCUT_H
#define	PILAMEVENTCUT_H

#include <AliFemtoBasicEventCut.h>

#include <utility>
#include <TH1D.h>
#include <TMap.h>

class PilamMultiAnalysis;

class PilamEventCut : public AliFemtoBasicEventCut {
public:
  PilamEventCut();
  PilamEventCut(PilamMultiAnalysis *parent, float centrality_low, float centrality_high);
  PilamEventCut(const char *name, const char *title);
  PilamEventCut(const char *name, const char *title, float centrality_low, float centrality_high);
  PilamEventCut(const char *name, const char *title, const TMap* options);
  PilamEventCut(const PilamEventCut& orig);
  virtual ~PilamEventCut();

  virtual bool Pass(const AliFemtoEvent* event);

  virtual TList *GetOutputList();

  void SetCentralityRange(float low, float high);

protected:

  PilamMultiAnalysis *_analysis;

  TString _name;
  TString _title_prefix;

  // std::pair<float,float> _centrality_range;
  float _cen_low;
  float _cen_high;

  TH1D *_centrality_pass;
  TH1D *_centrality_fail;
  

#ifdef __ROOT__
  ClassDef(PilamEventCut, 0)
#endif
  
};

#endif	/* PILAMEVENTCUT_H */

