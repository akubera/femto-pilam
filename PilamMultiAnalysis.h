

#ifndef PILAM_MULTI_ANALYSIS_H
#define PILAM_MULTI_ANALYSIS_H

#include <TList.h>
#include <TMap.h>

#ifndef __CINT__
#endif

#include <AliFemtoSimpleAnalysis.h>
#include <AliFemtoV0TrackCut.h>
#include <AliFemtoVertexMultAnalysis.h>

#include <AliFemtoBasicEventCut.h>
#include <AliFemtoBasicTrackCut.h>
#include <AliFemtoPairCut.h>
#include <AliFemtoDummyPairCut.h>

#include <AliFemtoCutMonitorEventMult.h>
#include <AliFemtoCutMonitorParticleYPt.h>
#include <AliFemtoCutMonitorV0.h>
#include <AliFemtoV0TrackPairCut.h>

class PilamEventCut;
class PilamPionCut;
class PilamLambdaCut;

class PilamMultiAnalysis : public AliFemtoVertexMultAnalysis {
public:
  enum ParticleType {kPiPlus, kPiMinus, kLambda, kAntiLambda};
  static const int PiPlusType = 0;
  static const int PiMinusType = 1;
  
public:
  PilamMultiAnalysis();
  PilamMultiAnalysis(const char *name);
  PilamMultiAnalysis(const std::string& name, float centrality_low, float centrality_high);
  PilamMultiAnalysis(ParticleType pitype, ParticleType lamtype, const std::string& name, const TMap& options);
  PilamMultiAnalysis(const PilamMultiAnalysis&);
  virtual TList* GetOutputList();

  const TString& GetName() const;

  void SetPionCut(AliFemtoParticleCut* cut) {
    SetSecondParticleCut(cut);
//    SetFirstParticleCut(cut);
  }

  void SetLambdaCut(AliFemtoV0TrackCut* cut) {
//    std::cout << "Setting lambda cut!!!\n";
    SetFirstParticleCut(cut);
//    SetSe condParticleCut(cut);
  }

#ifdef __ROOT__
  ClassDef(PilamMultiAnalysis, 2)
#endif

protected:
  TString _output_name;
  
  std::pair<double,double> centrality_range;
  
public:
//  AliFemtoBasicEventCut* event_cut;
  PilamEventCut *event_cut;
  AliFemtoBasicTrackCut *pion_cut;
  AliFemtoV0TrackCut *lambda_cut;
//  AliFemtoPairCut *pair_cut;
  AliFemtoV0TrackPairCut *pair_cut;

protected:
  void _SetupEventCut();
  void _SetupPionCut();
  void _SetupLambdaCut();
  void _SetupPairCut();

};

inline
const TString& PilamMultiAnalysis::GetName() const { return _output_name; };

#endif
