/* 
 * File:   PilamPairCutMonitor.cxx
 * Author: akubera
 * 
 * Created on October 10, 2014, 12:33 AM
 */

#include "PilamPairCutMonitor.h"

#include <TObjArray.h>

//#include <iostream>
//using namespace std;

#ifdef __ROOT__
 ClassImp(PilamPairCutMonitor)
#endif

PilamPairCutMonitor::PilamPairCutMonitor():
  AliFemtoCutMonitor()
{
  
  
}

PilamPairCutMonitor::PilamPairCutMonitor(const char *name, int nBins):
  AliFemtoCutMonitor(),
  _name(name),
  _dca(new TH1D("", "DcaInsideTpc", nBins, 0, 20)),
  _avg_sep(new TH1D("", "GetWeightedAvSep", nBins, 0, 20))
{
}

PilamPairCutMonitor::PilamPairCutMonitor(const PilamPairCutMonitor& orig):
  AliFemtoCutMonitor(orig),
  _dca((TH1D*)orig._dca->Clone()),
  _avg_sep((TH1D*)orig._avg_sep->Clone())
{
}

PilamPairCutMonitor::~PilamPairCutMonitor()
{
  delete _dca;
  delete _avg_sep;
}

TList*
PilamPairCutMonitor::GetOutputList()
{
  TList *olist = new TList();
  TObjArray *a = new TObjArray();
  a->SetName(_name.Data());
  a->Add(_dca);
  a->Add(_avg_sep);
  olist->Add(a);
  return olist;
}


double AliFemtoPair_DcaInsideTpc(const AliFemtoPair *pair)
{
   // dcs inside the STAR TPC
   double tMinDist = pair->NominalTpcEntranceSeparation();
   double tExit = pair->NominalTpcExitSeparation();
   tMinDist = (tExit>tMinDist) ? tMinDist : tExit;
   double tInsideDist;
   //tMinDist = 999.;

   double rMin = 60.;
   double rMax = 190.;
   const AliFmPhysicalHelixD& tHelix1 = pair->Track1()->Helix();
   const AliFmPhysicalHelixD& tHelix2 = pair->Track2()->Helix();
    
//   --- One is a line and other one a helix
   if (fabs(tHelix1.Curvature()) <= numeric_limits<double>::epsilon() ^ fabs(tHelix2.Curvature()) <= numeric_limits<double>::epsilon()) {
     return -999.;
   }
   // --- 2 lines : don't care right now
   //if (tHelix1.mSingularity)  return -999.;
   // --- 2 helix
   double dx = tHelix2.XCenter() - tHelix1.XCenter();
   double dy = tHelix2.YCenter() - tHelix1.YCenter();
   double dd = ::sqrt(dx*dx + dy*dy);
   double r1 = 1/tHelix1.Curvature();
   double r2 = 1/tHelix2.Curvature();
   double cosAlpha = (r1*r1 + dd*dd - r2*r2)/(2*r1*dd);
    
   double x, y, r;
   double s;
   if (fabs(cosAlpha) < 1) {           // two solutions
     double sinAlpha = sin(acos(cosAlpha));
     x = tHelix1.XCenter() + r1*(cosAlpha*dx - sinAlpha*dy)/dd;
     y = tHelix1.YCenter() + r1*(sinAlpha*dx + cosAlpha*dy)/dd;
     r = ::sqrt(x*x+y*y);
     if( r > rMin &&  r < rMax && 
 	fabs(atan2(y,x)-pair->Track1()->Track()->NominalTpcEntrancePoint().Phi())< 0.5
 	){ // first solution inside
       s = tHelix1.PathLength(x, y);
       tInsideDist=tHelix2.Distance(tHelix1.At(s));
       if(tInsideDist<tMinDist) tMinDist = tInsideDist;
     }
     else{ 
       x = tHelix1.XCenter() + r1*(cosAlpha*dx + sinAlpha*dy)/dd;
       y = tHelix1.YCenter() + r1*(cosAlpha*dy - sinAlpha*dx)/dd;
       r = ::sqrt(x*x+y*y);
       if( r > rMin &&  r < rMax &&
 	  fabs(atan2(y,x)-pair->Track1()->Track()->NominalTpcEntrancePoint().Phi())< 0.5
 	  ) {  // second solution inside
         s = tHelix1.PathLength(x, y);
         tInsideDist=tHelix2.Distance(tHelix1.At(s));
         if(tInsideDist<tMinDist) tMinDist = tInsideDist;
       }     
     }
   }
   return tMinDist;
 }

double AliFemtoPair_GetWeightedAvSep(const AliFemtoPair *pair)
{
  double result = 0.0;
//  pair->Track1()->
  return result;
}

void 
PilamPairCutMonitor::Fill(const AliFemtoPair* aPair)
{
  _dca->Fill(AliFemtoPair_DcaInsideTpc(aPair));
//  _avg_sep->Fill(aPair->GetWeightedAvSep());
}

PilamPairCutMonitor&
PilamPairCutMonitor::operator=(const PilamPairCutMonitor& aCut)
{
  return *this;
}
