/* 
 * File:   PilamPairCut.h
 * Author: akubera
 *
 * Created on September 29, 2014, 2:51 PM
 */

#ifndef PILAMPAIRCUT_H
#define	PILAMPAIRCUT_H

#ifndef __CINT__

#include <AliFemtoPairCut.h>
#include <AliFemtoPair.h>

#endif
#include <AliFemtoPairCut.h>
#include <TH1D.h>
#include "PilamMultiAnalysis.h"

class PilamPairCut : public AliFemtoV0TrackPairCut {
public:
  PilamPairCut();
  PilamPairCut(PilamMultiAnalysis*, const PilamMultiAnalysis::ParticleType&, const PilamMultiAnalysis::ParticleType&, TMap*);
  PilamPairCut(const PilamPairCut& orig);
  virtual ~PilamPairCut();

  virtual bool Pass(const AliFemtoPair* pair);

  virtual AliFemtoString Report();
  virtual TList *ListSettings();
  virtual TList *GetOutputList();

//  virtual AliFemtoPairCut* Clone();

  std::pair<double,double> lambda_purity_window;

protected:
  PilamMultiAnalysis::ParticleType _pion_type;
  PilamMultiAnalysis::ParticleType _lambda_type;

  TH1D *_lam_purity_hist;
  TH1D *_pi_purity_hist;

  TH1D *_minv_pass;
  TH1D *_minv_fail;

};

#endif	/* PILAMPAIRCUT_H */
