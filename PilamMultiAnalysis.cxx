
#include "PilamMultiAnalysis.h"
//#include "PilamPairCutMonitor.h"

#include "PilamEventCut.h"
#include "PilamPionCut.h"
#include "PilamLambdaCut.h"
#include "PilamPairCut.h"

#include "KuberCF.h"

#include <TObjArray.h>

#include <AliFemtoPairCut.h>
#include <AliFemtoShareQualityPairCut.h>

#include <AliESDtrack.h>
#include <TVector.h>

#ifndef __CINT__
#include <AliFemtoV0TrackPairCut.h>
#endif

#ifdef __ROOT__ 
ClassImp(AliFemtoVertexMultAnalysis)
#endif

static const double PionMass = 0.13956995,
                    KaonMass = 0.493677,
                  ProtonMass = 0.938272013,
                  LambdaMass = 1.115683;

//static const unsigned int binsVertex = 8;

PilamMultiAnalysis::PilamMultiAnalysis():
//  AliFemtoVertexMultAnalysis(),
  AliFemtoVertexMultAnalysis(8, -8.0, 8.0, 4, 0, 1000),
  _output_name("PilamMultiAnalysis"),
  centrality_range(0., 100.)
{
  SetVerboseMode(kFALSE);
}

PilamMultiAnalysis::PilamMultiAnalysis(const char *name)
{
  /** no-op **/
}

PilamMultiAnalysis::PilamMultiAnalysis(const std::string& name, float centrality_low, float centrality_high):
//  AliFemtoVertexMultAnalysis(),
  AliFemtoVertexMultAnalysis(8, -8.0, 8.0, 4, 0, 1000),
  _output_name(name.c_str()),
  centrality_range(0., 100.),
  event_cut(NULL),
//  event_cut(new PilamEventCut(this, centrality_low, centrality_high)),
//  event_cut(new AliFemtoBasicEventCut()),
  pion_cut(new AliFemtoBasicTrackCut()),
  lambda_cut(new AliFemtoV0TrackCut()),
  pair_cut(new AliFemtoV0TrackPairCut())
{
  event_cut = new PilamEventCut(this, centrality_low, centrality_high);

  SetVerboseMode(kFALSE);
  SetNumEventsToMix(10);
  SetMinSizePartCollection(1);

  _SetupEventCut();
  _SetupPionCut();
  _SetupLambdaCut();
  _SetupPairCut();

  SetEventCut(event_cut);
  SetPionCut(pion_cut);
  SetLambdaCut(lambda_cut);
  SetPairCut(pair_cut);
  
  char KCFName[] = "KCF";
  int cfunc_bin_count = 200;
  float kstar_min = 0.0, kstar_max = 1.0;

  KuberCF *kcf= new KuberCF(KCFName, cfunc_bin_count, kstar_min, kstar_max);
  kcf->SetAnalysis(this);
  AddCorrFctn(kcf);
}


PilamMultiAnalysis::PilamMultiAnalysis(ParticleType pitype, ParticleType lamtype, const std::string& name, const TMap& opts):
  AliFemtoVertexMultAnalysis(8, -8.0, 8.0, 4, 0, 1000),
  _output_name(name.c_str()),
  centrality_range(
    opts.GetValue("centrality") ? (*(TVectorD*)opts.GetValue("centrality"))[0] : 0,
    opts.GetValue("centrality") ? (*(TVectorD*)opts.GetValue("centrality"))[1] : 100),
//  {, (*(TVectorD*)opts.GetValue("centrality"))[1]} :  {0., 100.}),
  event_cut(new PilamEventCut(this, centrality_range.first, centrality_range.second)),
  pion_cut(new PilamPionCut(this, pitype)),
  lambda_cut(new PilamLambdaCut(this, lamtype)),
  pair_cut(new PilamPairCut(this, pitype, lamtype))
{
  SetVerboseMode(kFALSE);
  SetNumEventsToMix(10);
  SetMinSizePartCollection(1);

  _SetupEventCut();
  _SetupPionCut();
  _SetupLambdaCut();
  _SetupPairCut();

  SetEventCut(event_cut);
  SetPionCut(pion_cut);
  SetLambdaCut(lambda_cut);
  SetPairCut(pair_cut);
  
  char KCFName[] = "KCF";
  int cfunc_bin_count = 200;
  float kstar_min = 0.0, kstar_max = 1.0;

  KuberCF *kcf= new KuberCF(KCFName, cfunc_bin_count, kstar_min, kstar_max);
  kcf->SetAnalysis(this);
  AddCorrFctn(kcf);

}

PilamMultiAnalysis::PilamMultiAnalysis(const PilamMultiAnalysis& orig):
  AliFemtoVertexMultAnalysis(orig)
{
}

TList*
PilamMultiAnalysis::GetOutputList() {
//  cout << "[PilamMultiAnalysis::GetOutputList]\n";
  TList *olist = new TList(); // , *l = new TList();

  TObjArray *a = new TObjArray();
  a->SetName(_output_name.Data());
  olist->SetName(_output_name.Data());
  TList *real_output = AliFemtoSimpleAnalysis::GetOutputList();
//  cout << "[GetOutputList] " << _output_name.Data() << " : real_output has " << real_output->GetEntries() << " items.\n";

  TListIter next(real_output);
  while (TObject *obj = next()) {
    a->Add(obj);
  }
//  cout << '\n';
  delete real_output;
//  cout << "[GetOutputList] " << _output_name.Data() << " : a has " << a->GetEntries() << " items " << " : l has " << l->GetEntries() << " items.\n\n";

  olist->Add(a);
  return olist;
}

void
PilamMultiAnalysis::_SetupEventCut()
{
  event_cut->SetEventMult(0, 100000);
  event_cut->SetVertZPos(-20.0, 20.0);
//  event_cut->AddCutMonitor(new AliFemtoCutMonitorEventMult("_EvCut_Pass", 500),
//                         new AliFemtoCutMonitorEventMult("_EvCut_Fail", 500));

}

void
PilamMultiAnalysis::_SetupPionCut()
{
  pion_cut->SetMass(PionMass);
  pion_cut->SetNSigmaPion(-0.01, 0.01);
  pion_cut->SetPt(0.5, 4.5);
  pion_cut->SetRapidity(-0.8, 0.8);
  pion_cut->SetDCA(0.5, 5.0);
//  pion_cut->AddCutMonitor(new AliFemtoCutMonitorParticleYPt("_PiCut_Pass", PionMass),
//                          new AliFemtoCutMonitorParticleYPt("_PiCut_Fail", PionMass));
}

void
PilamMultiAnalysis::_SetupLambdaCut()
{
    lambda_cut->SetMass(LambdaMass);
    lambda_cut->SetEta(0.8); //0.8
    lambda_cut->SetPt(0.2, 5.0);
    lambda_cut->SetEtaDaughters(0.8); //0.8
    lambda_cut->SetPtPosDaughter(0.5,4.0); //0.5
    lambda_cut->SetPtNegDaughter(0.16,4.0); //0.16
    lambda_cut->SetTPCnclsDaughters(80); //80
    lambda_cut->SetNdofDaughters(4); //4.0
    lambda_cut->SetStatusDaughters(AliESDtrack::kTPCrefit/* | AliESDtrack::kITSrefit*/);
    lambda_cut->SetOnFlyStatus(kFALSE);
    lambda_cut->SetMaxDcaV0Daughters(0.4); //1.5 Jai, 0.6 //0.4
    lambda_cut->SetMaxDcaV0(0.5); //5.0
    lambda_cut->SetMaxV0DecayLength(100.0);
//    lambda_cut->SetMinDaughtersToPrimVertex(0.1); //0.01
    lambda_cut->SetMaxCosPointingAngle(0.9993); //0.99 - Jai //0.998
    lambda_cut->SetInvariantMassLambda(LambdaMass-0.2,LambdaMass+0.2);
//    lambda_cut->AddCutMonitor(new AliFemtoCutMonitorV0("_v0Cut_Pass"),
//                              new AliFemtoCutMonitorV0("_v0Cut_Fail"));
}

void
PilamMultiAnalysis::_SetupPairCut()
{
//  pair_cut->SetShareQualityMax(0.25)
//  pair_cut->SetShareFractionMax(0.1)
  pair_cut->SetTPCOnly(kTRUE);
  pair_cut->SetTPCExitSepMinimum(-1);
//  pair_cut->AddCutMonitor(new PilamPairCutMonitor("Pair_Pass", 288),
//                          new PilamPairCutMonitor("Pair_Fail", 288));
//  pair->SetMinAvgSeparation()
}

