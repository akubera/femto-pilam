#!/usr/bin/env bash

rm -f analysis_merge.jdl
rm -f analysis.jdl
rm -f analysis_validation_merge.sh
rm -f analysis_merge.sh
rm -f analysis_merge.C
rm -f analysis_validation.sh
rm -f analysis.sh
rm -f myAnalysis.C
rm -f analysis.root
rm -f Stage_*.xml

rm -f *.o
rm -f *.so
rm -f *.d
rm -f rm AutoDict*

rm -rf tempChecksum*
