#!/usr/bin/env bash


cat $1 | sed -e 's/, */\n/g' | sed '/^$/d' > $1.tmp

mv $1.tmp $1

