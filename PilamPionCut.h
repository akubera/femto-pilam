/* 
 * File:   PilamPionCut.h
 *
 */

#ifndef PILAM_PIONCUT_H
#define	PILAM_PIONCUT_H

#include <AliFemtoBasicTrackCut.h>

#include <utility>
#include <TH1D.h>
#include <TH2D.h>

#include "PilamMultiAnalysis.h"

class PilamPionCut : public AliFemtoBasicTrackCut {
public:
  PilamPionCut();
  PilamPionCut(PilamMultiAnalysis *parent, const PilamMultiAnalysis::ParticleType);
//  PilamPionCut(const PilamPionCut& orig);
  virtual ~PilamPionCut();

  virtual bool Pass(const AliFemtoTrack* track);

  virtual TList *GetOutputList();

protected:

  PilamMultiAnalysis *_analysis;
  PilamMultiAnalysis::ParticleType _type;

  TString _name;

  TH1D *_pion;
  TH1D *_minv;
  TH2D *_ypt_pass;
  TH2D *_ypt_fail;

#ifdef __ROOT__
  ClassDef(PilamPionCut, 0)
#endif

};

#endif	/* PILAM_PIONCUT_H */

