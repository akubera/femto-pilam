/* 
 * File:   PilamPairCutMonitor.h
 * Author: akubera
 *
 * Created on October 10, 2014, 12:33 AM
 */

#ifndef PILAMPAIRCUTMONITOR_H
#define	PILAMPAIRCUTMONITOR_H

#include <AliFemtoCutMonitor.h>
#include <AliFemtoPair.h>

#include <TH1D.h>
#include <TString.h>

class PilamPairCutMonitor : public AliFemtoCutMonitor {
public:
  PilamPairCutMonitor();
  PilamPairCutMonitor(const char *name, int nBins = 500);
  PilamPairCutMonitor(const PilamPairCutMonitor& orig);

  virtual ~PilamPairCutMonitor();

  PilamPairCutMonitor& operator=(const PilamPairCutMonitor& aCut);

  virtual void Fill(const AliFemtoPair* aPair);

  virtual TList *GetOutputList();

  virtual void Fill(const AliFemtoEvent* aEvent) {AliFemtoCutMonitor::Fill(aEvent);}
  virtual void Fill(const AliFemtoTrack* aTrack) {AliFemtoCutMonitor::Fill(aTrack);}
  virtual void Fill(const AliFemtoV0* aV0) {AliFemtoCutMonitor::Fill(aV0);}
  virtual void Fill(const AliFemtoKink* aKink) {AliFemtoCutMonitor::Fill(aKink);}
  virtual void Fill(const AliFemtoParticleCollection* aCollection) {AliFemtoCutMonitor::Fill(aCollection);}
  virtual void Fill(const AliFemtoEvent* aEvent,const AliFemtoParticleCollection* aCollection) {AliFemtoCutMonitor::Fill(aEvent, aCollection);}
  virtual void Fill(const AliFemtoParticleCollection* aCollection1,const AliFemtoParticleCollection* aCollection2) {AliFemtoCutMonitor::Fill(aCollection1, aCollection2);}

protected:

  TString _name;

  TH1D *_dca;
  TH1D *_avg_sep;

#ifdef __ROOT__
  ClassDef(PilamPairCutMonitor, 0)
#endif

};

#endif	/* PILAMPAIRCUTMONITOR_H */

