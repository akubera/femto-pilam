/* 
 * File:   PilamPionCut.cpp
 * Author: akubera
 * 
 * Created on October 13, 2014, 5:14 PM
 */

#include "PilamPionCut.h"
#include "PilamMultiAnalysis.h"

#include <TList.h>
#include <TObjArray.h>

#ifdef __ROOT__
  ClassImp(PilamPionCut)
#endif

static const double PionMass = 0.13956995;

static const int ypt_hist_x_numbins = 140,
                 ypt_hist_y_numbins = 100;

static const double ypt_hist_x_low  = -1.4,
                    ypt_hist_x_high = 1.4,
                    ypt_hist_y_low  = 0,
                    ypt_hist_y_high = 5;
  
PilamPionCut::PilamPionCut():
  AliFemtoBasicTrackCut(),
  _analysis(NULL),
  _type(PilamMultiAnalysis::kPiPlus),
  _name("PionCut"),
  _pion(NULL),
  _minv(NULL),
  _ypt_pass(NULL),
  _ypt_fail(NULL)
{
  /* no-op */
}

PilamPionCut::PilamPionCut(PilamMultiAnalysis *parent, const PilamMultiAnalysis::ParticleType type):
  AliFemtoBasicTrackCut(),
  _analysis(parent),
  _type(type),
  _name("PionCut"),
  _pion(new TH1D("Pion", "Pion information?", 100, 0, 10)),
  _minv(new TH1D("Energy", "Energy; E (GeV)", 288, 0, 10)),
  _ypt_pass(new TH2D("YPt_Pass", "Rapidity vs Pt (Pass)", ypt_hist_x_numbins, ypt_hist_x_low,ypt_hist_x_high,
        ypt_hist_y_numbins, ypt_hist_y_low,ypt_hist_y_high)),
  _ypt_fail(new TH2D("YPt_Fail", "Rapidity vs Pt (Fail)", ypt_hist_x_numbins, ypt_hist_x_low,ypt_hist_x_high,
        ypt_hist_y_numbins, ypt_hist_y_low,ypt_hist_y_high))
{
  switch (_type) {
    case PilamMultiAnalysis::kPiPlus:
      SetCharge(+1);
      break;
    case PilamMultiAnalysis::kPiMinus:
      SetCharge(-1);
      break;
    default:
      std::cerr << "Error : Incorrect pion type: " << _type;
      throw 1;
  }

}
/*
PilamPionCut::PilamPionCut(const PilamPionCut& orig):
   AliFemtoBasicTrackCut(const_cast<PilamPionCut&>(orig)),
  _analysis(orig._analysis),
  _type(orig._type)
{
  _pion = (TH1D)orig._pion->Clone();
}
*/
PilamPionCut::~PilamPionCut()
{
  delete _pion;
  delete _ypt_pass;
  delete _ypt_fail;
}

bool
PilamPionCut::Pass(const AliFemtoTrack* track)
{
  bool passed = AliFemtoBasicTrackCut::Pass(track);
  float pz = track->P().z();
  double energy = ::sqrt(track->P().Mag2() + PionMass * PionMass),
       rapidity = 0.5*::log((energy+pz)/(energy-pz));
//  (passed ? _ypt_pass : _ypt_fail)->Fill(rapidity, track->Pt());
  _minv->Fill(energy);
  (passed ? _ypt_pass : _ypt_fail)->Fill(rapidity, track->Pt());
  return passed;
}

TList*
PilamPionCut::GetOutputList()
{
//  cout << "[PilamPionCut::GetOutputList]\n";
  TList *olist = new TList();
  TList* monitor_list = AliFemtoCutMonitorHandler::GetOutputList();
  TObjArray *array = new TObjArray();
  array->SetName(_name.Data());
  TIter next(monitor_list);
  while (TObject *obj = next()) {
    array->Add(obj);
  }
  array->Add(_minv);
  array->Add(_ypt_pass);
  array->Add(_ypt_fail);
  olist->Add(array);
  return olist;
}
